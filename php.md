# ProCoders PHP Learning course

## Базовые видео курсы

[Дерево обучения](https://edu.cbsystematics.com/Images/RoadMap/Roadmap_Frontend-min.jpg)

Отдельная документация [о принципах разработки ПО](principles.md)

## Тестовые задания
+ [Тестовое задание №1](tasks/laravel/task1.md) - Простой CRUD + интерфейс для заполнения информации об учащийся ВУЗА

### 1. PHP

### 2. Общие курсы по экосистеме PHP

* Понимаем современные фреймворки с курсом [PSR-7 framework](https://elisdn.ru/blog/113/psr7-framework-http)
* [Как писать код](https://github.com/roistat/php-code-conventions)

Слушать видео курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)


### 3. Laravel framework

* Наиболее полный и современный [курс Laravel](https://nnm-club.me/forum/viewtopic.php?t=1217186) пишем сайт объявлений на Laravel от Дмитрий Елисеев
* Книга [Паттерны в Laravel](https://yadi.sk/i/qlC960zY1-ibLw)
* Разработка через тестирование [Test-Driven Laravel](https://coursehunters.net/course/adamwathan-test-driven-laravel)
* Обзор [40 лучших Laravel инструментов](https://senior.ua/articles/40-luchshih-instrumentov-i-resursov-laravel)

Краткий и понятный [учебник Laravel](http://unetway.com/tutorials/laravel/)

[Eloquent: Паттерны Производительности](https://coursehunter.net/course/eloquent-patterny-proizvoditelnosti)
[Практическое руководство по оптимизации кода Laravel](https://coursehunter.net/course/baselaravel-prakticheskoe-rukovodstvo-po-optimizacii-koda-laravel)

#### 3.3 Lararvel tools

* Интерактивная консоль [Laravel Tinker](https://laravel-news.com/laravel-tinker)

### 4. Yii Framework

Официальный сайт [Yii](https://www.yiiframework.com/)
* [Блог Yii2](https://elisdn.ru/blog/tag/Yii2) желательно прочесть все статьи
* Книга [Yii2 Cookbook](https://rutracker.org/forum/viewtopic.php?t=5314755)

### 5. Symfony

### 6. Тестирование
* [PHPUnit или тестирование для сомневающихся](https://habr.com/ru/post/485418/)

### 6. Deploy
* [Автоматическая проверка качества кода: 7 утилит](https://semaphoreci.com/blog/7-continuous-integration-tools-for-php-laravel)

#### 6.1 Docker

#### 6.3 Мониторинг 

* [Мониторинг мёртв? — Да здравствует мониторинг](https://habr.com/ru/company/itsumma/blog/448602/)
* [сбор данных — Prometheus + Grafana](https://medium.com/southbridge/prometheus-monitoring-ba8fbda6e83)
* анализ логов — ELK;
* для APM или Tracing — Jaeger (Zipkin).

