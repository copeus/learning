# React Admin

Рекомендуется полностью и вдумчиво прочитать (Documentation)[https://marmelab.com/react-admin/Readme.html] перед началом работы. 

Пример как струтурировать проект и код (React admin demo repository)[https://github.com/marmelab/react-admin/tree/master/examples/demo/src]

Однако для решения сложных связей (много-ко-многим)[https://marmelab.com/ra-enterprise/modules/ra-relationships]потребуется платная версия.
