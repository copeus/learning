# Devops

[DevOps Bootcamp](https://coursehunter.net/course/devops-bootcamp) - учебная программа для самостоятельного обучения, чтобы начать свою карьеру в качестве инженера DevOps.
68 часов на понятном англйиском

## Web

[Основы HTTPS, TLS, SSL](https://habr.com/ru/post/593507/). Создание собственных x509 сертификатов


## Linux shell

* Видео курс по [Bash](https://coursehunter.net/course/praktikum-bash-by-rebrain); 3 часа, основа основ
* [chmod и права доступа](https://habr.com/ru/post/469667/)



## Kubernetes
* [Кубик с нуля](https://coursehunter.net/course/kubernetes-s-nulya-dlya-devops-inzhenerov) на русском
* [Complete Kubernetes Tutorial for Beginners](https://www.youtube.com/watch?v=VnvRFRk_51k&list=PLy7NrYWoggjziYQIDorlXjTvvwweTYoNC&ab_channel=TechWorldwithNana) от Nana


## AWS
* [Как успешно подготовиться и сдать экзамен AWS Solutions Architect Associate](https://habr.com/ru/post/569436/)

## Terraform

* [Тераформ с нуля](https://coursehunter.net/course/terraform-s-nulya-do-professionala) на русском 2020 год
* [Полный курс по Terraform - C Нуля до Про](https://coursehunter.net/course/polnyy-kurs-terraform-ot-novichka-do-prodvinutomu) от Nana 2021 год
