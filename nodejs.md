# NodeJS

Общая [Roadmap for Backend developer](https://miro.medium.com/max/4800/1*kHFU0RqazHg1oXx8ylMuXA.png)
[NodeJS developer roadmap](https://github.com/aliyr/Nodejs-Developer-Roadmap/blob/master/Node.js-developer-roadmap.png?raw=true)

## Basic level
1. [ExpressJS](https://coursehunter.net/course/express-js-kurs-2019-ot-nulya-do-pervogo-servera-na-node-js) на русском
1. Кратко [Проектирование баз данных](https://coursehunter.net/course/proektirovanie-baz-dannyh) на русском. Обязательно к изучению 
1. [NestJS](https://coursehunter.net/course/nestjs-s-nulya-sovremennyy-backend-na-typescript-i-node-js) на русском
1. Практический курс [React и NestJS: Практическое Руководство С Docker](https://coursehunter.net/course/react-i-nestjs-prakticheskoe-rukovodstvo-po-docker)

## Advanced Level
1. [Моделирование данных Firestore](https://coursehunter.net/course/modelirovanie-dannyh-firestore)

## Architect Level

1. Глубокое понимание того, что [под капотом NodeJS](https://coursehunter.net/course/zhestkie-chasti-cervery-i-node-js) от харизматичного энергичного спикера
1. Полнейший курс из 6 частей [Базы данных](https://coursehunter.net/course/bazy-dannyh-chast-1) 96 часов о PostgreSQL, MySQL, Redis, MongoDB, Cassandra и т.д 
1. [Архитектура высоких нагрузок](https://coursehunter.net/course/arhitektor-vysokih-nagruzok) на русском


## Рецепты

### Web scraping и парсинг
- [Как написать правильный парсер](https://habr.com/ru/company/ruvds/blog/486688/)
- курс [Web scraping](https://coursehunter.net/course/web-scraping-v-nodejs) 
- пишем [поисковый робот](https://coursehunter.net/course/poiskovyy-robot-s-nodejs-h-m-amazon-linkedin-aliexpress)
