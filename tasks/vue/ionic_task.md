## Учебное задание

**Цель**: выяснить уровень знаний Vue 3/Ionic/GraphQL/TS

#### Запрещается использование VUEX/PINIA

### Предподготовка

Необходимо сделать реализовать приложение на Vue 3/Vue CLI.

#### Для интерффейса используем [@ionic/vue](https://www.npmjs.com/package/@ionic/vue)

#### Api client [vue-apollo](https://www.npmjs.com/package/vue-apollo)

#### Api 
+ Link: https://rickandmortyapi.com/graphql
+ Docs: https://rickandmortyapi.com/documentation

## ТЗ
Необходимо реализовать мобильное приложение с использованием Vue 3/Ionic/GraphQL/TS

### Страницы
+ /characters
+ /locations
+ /episodes
+ /characters/123
+ /episodes/123
+ /locations/123

##### На каждой странице должена быть навигация

### Страница списка должна выводить контент api, иметь возможность поиска по имени сущности и инфинити сколл
![Пример](assets/ionic_task/list_example.png)
#### Advanced - добавить скелетон лоадеры и фильтры которые предоставляет api
